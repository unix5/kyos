#include "tty.h"
#include "port.h"
#include "../klib/stddef.h"
#include "../klib/string.h"
#include "../kernel/mem.h"

#define VIDEO_POS(row, col) (2 * (row * 80 + col))
char TTY_ATTR;

enum TTY {
	TTY_MAX_ROW = 25, // Vertical
	TTY_MAX_COL = 80, // Horizontal

	TTY_VID_MEM = 0xb8000,
	TTY_VID_REG_CTRL = 0x3D4,
	TTY_VID_REG_DATA = 0x3D5,

	TTY_CURS_REG_HIGH = 14,
	TTY_CURS_REG_LOW = 15,
};

struct tty {
	size_t row;
	size_t col;

	char *video;
} tty = {
	.video = (char *)TTY_VID_MEM,
	.row = 0,
	.col = 0,
};

void tty_putchar(int row, int col, char chr, char attr)
{
	// Don't print characters after the screen memory
	if (row > TTY_MAX_ROW || (col >= TTY_MAX_COL && row == TTY_MAX_ROW))
		return;

	tty.video[VIDEO_POS(row, col)] = chr;
	tty.video[VIDEO_POS(row, col) + 1] = attr;
}

void tty_print(char chr)
{
	switch (chr) {
	case '\n':
		tty.row++;
		tty.col = 0;
		return;

	case '\r':
		tty.col = 0;
		return;
	case '\t':
		tty.col += 8;
		return;
	};

	if (tty.col > TTY_MAX_COL - 1) {
		tty.row++;
		tty.col = 0;
	}

	if (tty.row > TTY_MAX_ROW - 1) {
		tty.col = 0;
		tty.row--;

		for (int i = 0; i < TTY_MAX_ROW; ++i)
			strncpy(&tty.video[i * 160],
				&tty.video[160 + (i * 160)], 160);

		for (int i = 0; i < TTY_MAX_COL; ++i)
			tty_putchar(tty.row, i, ' ', 0);
	}

	tty_putchar(tty.row, tty.col++, chr, TTY_ATTR);
	tty_set_curs();
}

void tty_clean(void)
{
	for (int i = 0; i <= VIDEO_POS(TTY_MAX_ROW, TTY_MAX_COL); i += 2) {
		tty.video[i] = ' ';
		tty.video[i + 1] = TTY_WHITE_BLACK;
	}

	tty.col = 0;
	tty.row = 0;
}

void tty_set_curs(void)
{
	port_word_out(TTY_VID_REG_CTRL, TTY_CURS_REG_HIGH);
	port_word_out(TTY_VID_REG_DATA, (tty.row * 80) + tty.col);
}
