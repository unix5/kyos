#ifndef TTY_H
#define TTY_H

#define TTY_SET_ATTR(bg, fg) (TTY_ATTR = (bg << 4 | fg))
extern char TTY_ATTR;

enum TTY_COLORS {
	TTY_WHITE_BLACK = 0x0F,
	TTY_BLACK = 0x00,
	TTY_BLUE = 0x01,
	TTY_GREEN = 0x02,
	TTY_CYAN = 0x03,
	TTY_RED = 0x04,
	TTY_MAGENTA = 0x05,
	TTY_BROWN = 0x06,
	TTY_LIGHT_GRAY = 0x07,
	TTY_DARK_GRAY = 0x08,
	TTY_LIGHT_BLUE = 0x09,
	TTY_LIGHT_GREEN = 0x10,
	TTY_LIGHT_CYAN = 0x11,
	TTY_LIGHT_RED = 0x12,
	TTY_LIGHT_MAGENTA = 0x13,
	TTY_YELLOW = 0x14,
	TTY_WHITE = 0x15,
};

void tty_putchar(int row, int col, char chr, char attr);
void tty_print(char chr);
void tty_clean(void);

void tty_set_curs(void);

#endif /* TTY_H */
