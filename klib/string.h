#ifndef STRING_H
#define STRING_H

#include "stddef.h"

// Normal version
/**
 * @fn strcpy
 * @brief Copy string from src to dest.
 * 
 * @param dest Where the string will be stored.
 * @param src String to be copied.
 * @return int
 */
int strcpy(char *dest, char *src);

/**
 * @fn strcmp
 * @brief Compare two strings.
 * 
 * @param a first string.
 * @param b second string.
 * @param n ammount of bytes to be copied.
 * @return int 0 for unequal, 1 for equal.
 */
int strcmp(char *a, char *b);

/**
 * @fn strlen
 * @brief Get the size of a string.
 * 
 * @param src String to get the size.
 * @return size_t Size of the string.
 */
size_t strlen(char *src);

// Size limited version
/**
 * @fn strncpy
 * @brief Copy string from src to dest.
 * 
 * @param dest Where the string will be stored.
 * @param src String to be copied.
 * @param n ammount of bytes to be copied.
 * @return int
 */
int strncpy(char *dest, char *src, size_t n);

/**
 * @fn strncmp
 * @brief Compare two strings
 * 
 * @param target first string
 * @param src second string
 * @param n ammount of bytes to be copied.
 * @return int 0 for unequal, 1 for equal.
 */
int strncmp(char *a, char *b, size_t n);

/**
 * @fn strnlen
 * @brief Get the size of a string.
 * 
 * @param src String to get the size.
 * @param n max size of a string
 * @return size_t Size of the string.
 */
size_t strnlen(char *src, size_t n);

#endif /* STRING_H */
