#include "string.h"
#include "stddef.h"

int strcpy(char *dest, char *src)
{
	while (*src != '\0')
		*dest++ = *src++;

	return 0;
}

int strncpy(char *dest, char *src, size_t n)
{
	for (int i = 0; i < n; ++i)
		*dest++ = *src++;

	return 0;
}

int strcmp(char *a, char *b)
{
	for (; *b != '\0'; ++b, ++a) {
		if (*a != *b)
			return 0;
	}

	return 1;
}

int strncmp(char *a, char *b, size_t n)
{
	for (int i = 0; i < n; ++i) {
		if (*a++ != *b++)
			return 0;
	}

	return 1;
}

size_t strlen(char *src)
{
	size_t r = 0;

	while (src[r++] != '\0')
		;

	return r;
}

size_t strnlen(char *src, size_t n)
{
	size_t r = 0;

	while(src[r++] != '\0' && r < n)
		;

	return r;
}
