#include "stdio.h"
#include "stdarg.h"
#include "../drivers/tty.h"

void kprint(char *fmt, ...)
{
	va_list argv;
	va_start(argv, fmt);

	for(; *fmt != '\0'; ++fmt) {
		if(*fmt == '%') {
			fmt++;
			switch(*fmt) {
			case '%':
				tty_print(*fmt);
				break;
			case 'd':
				break;
			case 'h':
				break;
			case 'b':
				break;
			case 'c':
				tty_print(va_arg(argv, int));
				
				break;
			case 's': {
				char *str = va_arg(argv, char *);
				for(int i = 0; str[i]; ++i)
					tty_print(str[i]);

				break;
			}
			}
		}
		else
			tty_print(*fmt);
	}

	va_end(argv);
}
