#include "./stdlib.h"
#include "./stddef.h"

typedef struct m_block m_block_t;

struct m_block {
	void *data;

	char free;
	size_t size;

	m_block_t *next, *prev;
};

m_block_t *heap = NULL;


void *malloc(size_t size)
{
	void *ptr = NULL;

	return ptr;
}

void *calloc(size_t nmemb, size_t size)
{
	return NULL;
}

void *realloc(void *ptr, size_t size) 
{
	return NULL;
}

void free(void *ptr) 
{
	return;
}
