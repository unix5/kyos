arch ?= x86_64
kernel := build/vmkyos
iso := build/hello.iso

ld_script := linker.ld
grub_cfg := grub.cfg

asm_src := $(wildcard asm/*.asm)
c_src := $(wildcard kernel/*.c klib/*.c drivers/*.c)

OBJS = $(asm_src:%=build/%.o) $(c_src:%=build/%.o)

.PHONY: all clean run iso kernel

all: $(kernel)

$(kernel): $(OBJS) $(ld_script)
	i686-elf-gcc -T $(ld_script) -o $(kernel) -ffreestanding -O2 -nostdlib $(OBJS) -lgcc

# compile c files
build/%.c.o: %.c
	@mkdir -p $(shell dirname $@)
	i686-elf-gcc -std=gnu99 -ffreestanding -O2 -c $< -o $@

# compile asm files
build/%.asm.o: %.asm
	@mkdir -p $(shell dirname $@)
	i686-elf-as -g $< -o $@

run: $(iso)
	qemu-system-x86_64 -cdrom $(iso)

iso: $(iso)
	@echo "Done"

$(iso): $(kernel) $(grub_cfg)
	mkdir -p build/isofiles/boot/grub
	cp $(kernel) build/isofiles/boot/vmkyos
	cp $(grub_cfg) build/isofiles/boot/grub
	grub-mkrescue -o $(iso) build/isofiles #2> /dev/null

clean:
	@rm -fr build
