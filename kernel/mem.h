#ifndef MEM_H
#define MEM_H

#include "../klib/stddef.h"

void kmemset(char *dest, char chr, size_t n);

#endif /* MEM_H */
