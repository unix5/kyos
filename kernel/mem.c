#include "mem.h"
#include "../klib/stddef.h"

void kmemset(char *dest, char chr, size_t n)
{
	for(int i = 0; i < n; ++i)
		dest[i] = chr;
}
